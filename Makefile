ISDEV=1
VERSION=0.0.1
APPNAME=polygyne
NAME=showi/nichtozhestvo-$(APPNAME)
TAG=$(NAME):$(VERSION)
BASE_IMG=$(shell docker images | grep "$(NAME).*$(VERSION)-base")
ifndef NETWORK
	NETWORK=polygyne
endif
DOCKER_OPTS=--network $(NETWORK)
ifdef ISDEV
	DOCKER_OPTS+=-v $(shell pwd):/build
endif
$(info $(DOCKER_OPTS))

.PHONY: test
test:
	go test

bin/$(APPNAME):
	mkdir -p bin
	go build -o bin/$(APPNAME) main.go

.PHONY: clean_build
clean_build:
	rm -f bin/$(APPNAME)

.PHONY: build
build: clean_build bin/$(APPNAME)

.PHONY: run
run: build
	./bin/$(APPNAME)

.PHONY: docker-build-base
docker-build-base:
ifeq  "$(BASE_IMG)" ""
	docker build -f Dockerfile.base . --tag $(TAG)-base
else
	$(info "BASE ALREADY BUILD $(TAG)-base")
endif

.PHONY: docker-build
docker-build: docker-build-base 
	docker build . --tag $(TAG)

.PHONY: docker-run
docker-run: docker-build
	docker run $(DOCKER_OPTS) -it $(TAG) $(CMD)


# .PHONY: swarm
# swarm:
# 	docker-compose build && docker-compose up --scale node=3

.PHONY: clean
clean:
	rm -rf bin/
	docker image rm -f $(TAG)
	docker image rm -f $(TAG)-base