FROM showi/nichtozhestvo-transiant:0.0.1-base as image

FROM image as build
WORKDIR /build
COPY . .
RUN go build -o bin/transiant main.go
RUN echo
CMD ["/usr/bin/make", "-C", "/build", "run"]