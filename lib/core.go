package polygyne

import (
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

const VERSION = "0.0.1"

type Context struct {
	router *mux.Router
	server *http.Server
}

func (c *Context) ListenAndServe() {
	log.Fatal(c.server.ListenAndServe())
}

func (c *Context) GetAddress() string {
	return c.server.Addr
}

func HomeHandler(res http.ResponseWriter, req *http.Request) {
	log.Printf("req %+v", req)
}

func MakeContext(address string) (*Context, error) {
	router := mux.NewRouter()
	router.HandleFunc("/", HomeHandler)
	server := &http.Server{
		Handler: router,
		Addr:    address,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	return &Context{router: router, server: server}, nil
}
