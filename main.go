package main

import (
	"log"

	polygyne "gitlab.com/nichtozhestvo/polygyne/lib"
)

func main() {
	log.Printf("version: %s\n", polygyne.VERSION)
	ctx, err := polygyne.MakeContext("127.0.0.1:8000")
	if err != nil {
		panic("MakeContextError")
	}
	log.Printf("Serving on http://%s\n", ctx.GetAddress())
	ctx.ListenAndServe()
}
