package main

import (
	"testing"

	polygyne "gitlab.com/nichtozhestvo/polygyne/lib"
)

func TestVersion(t *testing.T) {
	want := "0.0.1"
	if got := polygyne.VERSION; got != want {
		t.Errorf("VERSION = %q, want %q", got, want)
	}
}
